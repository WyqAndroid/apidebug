package com.android.apishow

import android.annotation.SuppressLint
import android.os.Environment
import android.text.TextUtils
import com.addcn.android.hk591new.util.AesEncryptUtil
import java.io.File
import java.text.SimpleDateFormat
import java.util.*

/**
 * 创建人： 汪勇奇
 * 创建时间：2018/7/16.
 * 文件说明：Api数据集合
 */

class ApiList {

    private constructor() {
        //  初始化默认数据
        initData()
        Thread {
            kotlin.run {
                // 加载日志数据
                loadLogData()
            }
        }.start()
    }

    companion object {

        private var mInstance: ApiList? = null

        val instance: ApiList
            get() {
                if (this.mInstance == null) {
                    this.mInstance = ApiList()
                }
                return mInstance!!
            }
    }

    // 默认API数据集合
    private lateinit var apiList: MutableList<Api>

    fun getApiList(): MutableList<Api> {
        return apiList
    }

    // 日志API数据集合
    private lateinit var logList: MutableList<Api>

    fun getLogList(): MutableList<Api> {
        return logList
    }

    // 获取API对象
    private fun getApi(position: Int, name: String, type: String, url: String, parameter: String, description: String): Api {
        val api = Api()
        api.name = ("" + name)
        api.type = ("" + type)
        api.url = ("" + url)
        api.parameter = ("" + parameter)
        api.description = ("" + description)
        api.position = position
        return api
    }

    // 初始化默认API数据
    private fun initData() {
        apiList = ArrayList()
        apiList.add(getApi(0, "天气预报", "Get", "http://tj.nineton.cn/Heart/index/all", "", "案例示范：查看JSON视图模板"))
        apiList.add(getApi(1, "启动页", "Get", "https://www.xxx.com/********************", "", "这个接口是做xxx，字段a代表xxx"))
        apiList.add(getApi(2, "首页主功能", "Get", "https://www.xxx.com/********************", "", "这个接口是做xxx，字段a代表xxx"))
        apiList.add(getApi(3, "订阅列表", "Get", "https://www.xxx.com/********************", "", "这个接口是做xxx，字段a代表xxx"))
        apiList.add(getApi(4, "消息推送", "Post", "https://www.xxx.com/********************", "", "这个接口是做xxx，字段a代表xxx"))
        apiList.add(getApi(5, "消息提醒", "Get", "https://www.xxx.com/********************", "", "这个接口是做xxx，字段a代表xxx"))
        apiList.add(getApi(6, "新闻频道", "Get", "https://www.xxx.com/********************", "", "这个接口是做xxx，字段a代表xxx"))
        apiList.add(getApi(7, "详情页", "Get", "https://www.xxx.com/********************", "", "这个接口是做xxx，字段a代表xxx"))
    }

    // 加载日志数据
    @SuppressLint("SimpleDateFormat")
    fun loadLogData() {
        synchronized(this) {
            logList = ArrayList()
            val fileDir = File(Environment.getExternalStorageDirectory().canonicalPath + "/Log/encrypt/")
            if (fileDir.exists()) {
                val subFile = fileDir.listFiles()
                for (iFileLength in subFile.indices) {
                    val curFile = subFile[iFileLength]
                    // 判断是否为文件夹
                    if (!curFile.isDirectory) {
                        val fileName = subFile[iFileLength].name
                        val text = AesEncryptUtil.aesDecodeText("ZheLiXieNiDeMiYao", "/Log/encrypt/", fileName)
                        val textArray = text.split(";")
                        var api = Api()
                        for (str in textArray) {
                            if (str.contains("=")) {
                                val prefix = str.subSequence(0, str.indexOf("="))
                                val content = str.subSequence(str.indexOf("=") + 1, str.length)
                                when (prefix) {
                                    "type" -> {
                                        api.type = content.toString()
                                    }
                                    "url" -> {
                                        api.url = content.toString()
                                    }
                                    "parameter" -> {
                                        api.parameter = content.toString()
                                    }
                                    "time" -> {
                                        val sdf = SimpleDateFormat("yyyy年MM月dd日 HH:mm:ss")
                                        val time = content.toString().toLong()
                                        api.time = time
                                        api.date = sdf.format(time)
                                    }
                                }
                                api.file = curFile
                            }
                        }
                        if (!TextUtils.isEmpty(api.url)) {
                            logList.add(api)
                        }
                    }
                }
            }
            if (logList.isNotEmpty()) {
                Collections.sort(logList, SortComparator("desc"))
            }
        }
    }

}