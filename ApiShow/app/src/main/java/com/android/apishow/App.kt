package com.android.apishow

import android.app.Application
import com.android.apishow.request.HttpClientHelper

/**
 * 创建人： 汪勇奇
 * 创建时间：2018/7/18.
 * 文件说明：
 */

class App : Application() {

    override fun onCreate() {
        super.onCreate()
        init(this)
    }

    companion object {

        private lateinit var mApp: Application

        private fun init(app: Application) {
            mApp = app
            HttpClientHelper.initHttpClient(app)
        }

        internal val context: Application
            get() {
                return mApp
            }
    }

}