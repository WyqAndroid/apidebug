package com.android.apishow.request;

import android.content.Context;
import android.os.Build;
import android.text.TextUtils;

import com.android.apishow.request.store.CookieJarImpl;
import com.android.apishow.request.store.PersistentCookieStore;

import java.io.IOException;
import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

/**
 * 项目名称：<br>
 * 类名称：HttpClientHelper<br>
 * 类描述：网络请求数据<br>
 * 创建人：汪勇奇<br>
 * 创建时间：2016年9月23日下午3:33:34<br>
 * 修改人： <br>
 * 修改时间： <br>
 * 修改备注：
 *
 * @version V1.0
 */

public class HttpClientHelper {

    /**
     * OkHttpClient
     **/
    private static OkHttpClient okHttpClient;
    private static PersistentCookieStore persistentCookieStore;

    private final static int READ_TIMEOUT = 60;
    private final static int WRITE_TIMEOUT = 60;
    private final static int CONNECT_TIMEOUT = 60;

    public static void initHttpClient(Context context) {
        if (okHttpClient == null) {
            OkHttpClient.Builder builder = new OkHttpClient.Builder();
            builder.readTimeout(READ_TIMEOUT, TimeUnit.SECONDS);//设置读取超时时间
            builder.writeTimeout(WRITE_TIMEOUT, TimeUnit.SECONDS);//设置写的超时时间
            builder.connectTimeout(CONNECT_TIMEOUT, TimeUnit.SECONDS);//设置连接超时时间
            persistentCookieStore = new PersistentCookieStore(context);
            CookieJarImpl cookieJarImpl = new CookieJarImpl(persistentCookieStore);
            builder.cookieJar(cookieJarImpl);
            okHttpClient = builder.build();
        }
    }

    public static String doGet(String url) {
        String result = "";
        Request request = new Request.Builder()
                .url(url)
                .addHeader("content-type", "application/json;charset:utf-8")
                .addHeader("user-agent", encodeHeadInfo(getUserAgent()))
                .build();
        try {
            if (okHttpClient != null) {
                Response response = okHttpClient.newCall(request).execute();
                if (response.isSuccessful()) {
                    result = response.body().string();
                    response.close();
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return result;
    }

    public static String doPost(String url, Map<String, String> params) {
        // 如果是指定内部测试人员开启的debug模式
        String result = "";
        FormBody.Builder bodyBuilder = new FormBody.Builder();
        try {
            if (params != null) {
                Iterator<Map.Entry<String, String>> iterator = params.entrySet().iterator();
                while (iterator.hasNext()) {
                    Map.Entry<String, String> entry = iterator.next();
                    String key = entry.getKey() + "";
                    String value = entry.getValue() + "";
                    if (!TextUtils.isEmpty(key)) {
                        bodyBuilder.add(key, value);
                    }
                }
            }
        } catch (Exception e) {
        }
        Request request = new Request.Builder().url(url)
                .addHeader("content-type", "application/json;charset:utf-8")
                .addHeader("user-agent", encodeHeadInfo(getUserAgent()))
                .post(bodyBuilder.build())
                .build();
        try {
            if (okHttpClient != null) {
                Response response = okHttpClient.newCall(request).execute();
                if (response.isSuccessful()) {
                    result = response.body().string();
                    response.close();
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return result;
    }

    private static String getUserAgent() {
        StringBuilder builder = new StringBuilder("com.addcn.android");
        builder.append('/' + "2.20.6.93");
        builder.append("/720x1280");
        builder.append("/Android");
        builder.append("/" + Build.VERSION.RELEASE);
        builder.append("/" + Build.MODEL);
        builder.append("/353025073161556");
        return builder.toString();
    }

    private static String encodeHeadInfo(String headInfo) {
        StringBuffer stringBuffer = new StringBuffer();
        try {
            if (!TextUtils.isEmpty(headInfo)) {
                int length = headInfo.length();
                for (int i = 0; i < length; i++) {
                    char c = headInfo.charAt(i);
                    if (c <= '\u001f' || c >= '\u007f') {
                        stringBuffer.append(String.format("\\u%04x", (int) c));
                    } else {
                        stringBuffer.append(c);
                    }
                }
            }
        } catch (Exception ex) {
            return "" + headInfo;
        }
        return stringBuffer.toString();
    }

}
