package com.android.apishow

import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import java.util.*

/**
 * 创建人： 汪勇奇
 * 创建时间：2018/7/16.
 * 文件说明：Api列表适配器
 */

class ApiListAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder> {

    var isLogApi = false
    var list: MutableList<Api>
    private var mContext: Context
    private var mInflater: LayoutInflater
    private lateinit var mItemClickListener: OnListItemClickListener

    constructor(context: Context) {
        mContext = context
        mInflater = LayoutInflater.from(context)
        list = ArrayList()
    }

    fun setOnItemClickListener(itemClickListener: OnListItemClickListener) {
        this.mItemClickListener = itemClickListener
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        var view = mInflater.inflate(R.layout.item_api_list, null) as View
        var listHolder = ListHolder(view)
        listHolder.tvName = view.findViewById(R.id.tv_name)
        listHolder.layoutName = view.findViewById(R.id.layout_name)
        listHolder.tvType = view.findViewById(R.id.tv_type)
        listHolder.tvUrl = view.findViewById(R.id.tv_url)
        listHolder.tvParameter = view.findViewById(R.id.tv_parameter)
        listHolder.layoutParameter = view.findViewById(R.id.layout_parameter)
        listHolder.tvTime = view.findViewById(R.id.tv_time)
        listHolder.layoutTime = view.findViewById(R.id.layout_time)
        listHolder.tvDescription = view.findViewById(R.id.tv_description)
        listHolder.layoutDescription = view.findViewById(R.id.layout_description)
        listHolder.tvPostMan = view.findViewById(R.id.tv_postMan)
        listHolder.viewLine = view.findViewById(R.id.view_line)
        listHolder.layoutItem = view.findViewById(R.id.layout_item)
        return listHolder
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is ListHolder) {
            if (list.size > position) {
                var api = list[position]
                api.position = position
                if (TextUtils.isEmpty(api.name)) {
                    holder.layoutName.visibility = View.GONE
                } else {
                    holder.tvName.text = api.name
                    holder.layoutName.visibility = View.VISIBLE
                }

                holder.tvType.text = api.type

                holder.tvUrl.text = api.url

                if (TextUtils.isEmpty(api.parameter)) {
                    holder.layoutParameter.visibility = View.GONE
                } else {
                    holder.tvParameter.text = api.parameter
                    holder.layoutParameter.visibility = View.VISIBLE
                }

                if (TextUtils.isEmpty(api.date)) {
                    holder.layoutTime.visibility = View.GONE
                } else {
                    holder.tvTime.text = api.date
                    holder.layoutTime.visibility = View.VISIBLE
                }

                if (TextUtils.isEmpty(api.description)) {
                    holder.layoutDescription.visibility = View.GONE
                } else {
                    holder.tvDescription.text = api.description
                    holder.layoutDescription.visibility = View.VISIBLE
                }

                if (api.type == "Get" || api.type == "Post") {
                    holder.tvPostMan.visibility = View.VISIBLE
                    holder.tvPostMan.setOnClickListener {
                        //获取剪贴板管理器
                        // val cm = mContext.getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager
                        // 创建普通字符型ClipData
                        // val mClipData = ClipData.newPlainText("Label", "" + api.toString())
                        // 将ClipData内容放到系统剪贴板里。
                        // cm.primaryClip = mClipData
                        val intent = Intent()
                        intent.putExtra("name", api.name)
                        intent.putExtra("type", api.type)
                        intent.putExtra("url", api.url)
                        intent.putExtra("parameter", api.parameter)
                        intent.setClass(mContext, PostmanActivity::class.java)
                        mContext.startActivity(intent)
                    }
                } else {
                    holder.tvPostMan.visibility = View.GONE
                }

                if (list.size == position + 1) {
                    holder.viewLine.visibility = View.GONE
                } else {
                    holder.viewLine.visibility = View.VISIBLE
                }

                holder.layoutItem.setOnClickListener {
                    if (mItemClickListener != null) {
                        mItemClickListener.onListItemClickListener(api)
                    }
                }
                holder.layoutItem.setOnLongClickListener {
                    if (isLogApi) {
                        AlertDialog.Builder(mContext)
                                .setMessage("是否确定永久删除该日志API数据？")
                                .setPositiveButton("确定") { _, _ ->
                                    api.file!!.delete()
                                    list.remove(api)
                                    notifyDataSetChanged()
                                    Toast.makeText(mContext, "删除成功！", Toast.LENGTH_SHORT).show()
                                }
                                .setNegativeButton("取消", null)
                                .create()
                                .show()
                    }
                    true
                }
            }
        }
    }

    private class ListHolder : RecyclerView.ViewHolder {

        constructor(view: View) : super(view)

        lateinit var tvName: TextView
        lateinit var layoutName: LinearLayout

        lateinit var tvType: TextView

        lateinit var tvUrl: TextView

        lateinit var tvParameter: TextView
        lateinit var layoutParameter: LinearLayout

        lateinit var tvTime: TextView
        lateinit var layoutTime: LinearLayout

        lateinit var tvDescription: TextView
        lateinit var layoutDescription: LinearLayout

        lateinit var tvPostMan: TextView

        lateinit var viewLine: View
        lateinit var layoutItem: LinearLayout

    }

}