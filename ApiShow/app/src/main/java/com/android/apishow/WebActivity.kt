package com.android.apishow

import android.annotation.SuppressLint
import android.graphics.Color
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.text.TextUtils
import android.view.View
import android.webkit.WebSettings
import android.widget.ImageView
import android.widget.TextView
import com.android.apishow.request.ApiRequestHelper
import com.android.apishow.request.JSONTemplate
import thereisnospon.codeview.CodeView
import thereisnospon.codeview.CodeViewTheme
import java.math.BigDecimal

/**
 * 创建人： 汪勇奇
 * 创建时间：2018/7/16.
 * 文件说明：网页内嵌
 */

class WebActivity : AppCompatActivity() {

    @SuppressLint("SetJavaScriptEnabled", "Range")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_web)

        val name = intent.getStringExtra("name")
        val type = intent.getStringExtra("type")
        val url = intent.getStringExtra("url")
        var parameter = intent.getStringExtra("parameter")
        val page = intent.getStringExtra("page")

        // 标题
        val tvTitle = findViewById<TextView>(R.id.tv_title)
        if (!TextUtils.isEmpty(name)) {
            tvTitle.text = name
        }

        // 返回按钮
        val ivBack = findViewById<ImageView>(R.id.iv_back)
        ivBack.setOnClickListener {
            finish()
        }

        // 结果展示控件
        val cvResult = findViewById<CodeView>(R.id.cv_result)
        cvResult.setTheme(CodeViewTheme.ANDROIDSTUDIO).fillColor()
        cvResult.setEncode("UTF-8")
        cvResult.isVerticalScrollBarEnabled = true
        cvResult.isHorizontalScrollBarEnabled = false

        // 设置 WebView中启用文件访问
        cvResult.settings.allowFileAccess = true
        // 设置 WebView启用JavaScript执行
        cvResult.settings.javaScriptEnabled = true
        // 设置WebView是否应启用对“视口”HTML元标记的支持或应使用宽视口
        cvResult.settings.useWideViewPort = true
        // 设置WebView是否以概览模式加载页面，即缩小宽度以适合屏幕的内容
        cvResult.settings.loadWithOverviewMode = true
        //设置 WebView 编码格式
        cvResult.settings.defaultTextEncodingName = "UTF-8"
        // 设置 WebView 字体的大小，默认大小为 16
        cvResult.settings.defaultFontSize = 20
        // 设置 WebView 支持的最小字体大小，默认为 8
        cvResult.settings.minimumFontSize = 14
        // 设置 WebView 通过JS打开新窗口
        cvResult.settings.javaScriptCanOpenWindowsAutomatically = true
        // 禁止横向滚动
        cvResult.settings.layoutAlgorithm = WebSettings.LayoutAlgorithm.SINGLE_COLUMN

        // 结果展示控件
        val tvResult = findViewById<TextView>(R.id.tv_result)
        // 时间展示控件
        val tvTime = findViewById<TextView>(R.id.tv_time)
        // 接口请求的开始时间
        val startTime = System.currentTimeMillis()

        when (type) {
            "Post" -> {
                parameter = parameter.replace("{", "")
                        .replace("}", "")
                        .replace(" ", "")
                val map = HashMap<String, String>()
                val textArray = if (page == "postman") (parameter.split("#")) else parameter.split(",")

                for (str in textArray) {
                    if (str.contains("=")) {
                        val key = str.subSequence(0, str.indexOf("="))
                        val value = str.subSequence(str.indexOf("=") + 1, str.length)
                        map[key.toString()] = value.toString()
                    }
                }
                cvResult.visibility = View.GONE
                tvResult.visibility = View.VISIBLE
                ApiRequestHelper.getInstance().doPost(url, map) { result ->
                    val time = System.currentTimeMillis() - startTime
                    val text = (BigDecimal(time) * BigDecimal(0.001)).setScale(3, BigDecimal.ROUND_HALF_UP)
                    tvTime.text = "接口请求所消耗时长： ${text}秒"
                    when {
                        time > 3000 -> {
                            // 红色
                            tvTime.setTextColor(Color.parseColor("#ff0000"))
                        }
                        time > 2000 -> {
                            // 紫色
                            tvTime.setTextColor(Color.parseColor("#aa00ff"))
                        }
                        time > 1000 -> {
                            // 黄色
                            tvTime.setTextColor(Color.parseColor("#ffd600"))
                        }
                        time > 500 -> {
                            // 蓝色
                            tvTime.setTextColor(Color.parseColor("#00b0ff"))
                        }
                        else -> {
                            // 绿色
                            tvTime.setTextColor(Color.parseColor("#00c853"))
                        }
                    }
                    if (!TextUtils.isEmpty(result)) {
                        tvResult.text = JSONTemplate.parseJson(result)
                    } else {
                        tvResult.text = "没有返回数据"
                    }
                }
            }
            "Get" -> {
                cvResult.visibility = View.GONE
                tvResult.visibility = View.VISIBLE
                ApiRequestHelper.getInstance().doGet(url) { result ->
                    val time = System.currentTimeMillis() - startTime
                    val text = (BigDecimal(time) * BigDecimal(0.001)).setScale(3, BigDecimal.ROUND_HALF_UP)
                    tvTime.text = "接口请求所消耗时长： ${text}秒"
                    when {
                        time > 3000 -> {
                            // 红色
                            tvTime.setTextColor(Color.parseColor("#ff0000"))
                        }
                        time > 2000 -> {
                            // 紫色
                            tvTime.setTextColor(Color.parseColor("#aa00ff"))
                        }
                        time > 1000 -> {
                            // 黄色
                            tvTime.setTextColor(Color.parseColor("#ffd600"))
                        }
                        time > 500 -> {
                            // 蓝色
                            tvTime.setTextColor(Color.parseColor("#00b0ff"))
                        }
                        else -> {
                            // 绿色
                            tvTime.setTextColor(Color.parseColor("#00c853"))
                        }
                    }
                    if (!TextUtils.isEmpty(result)) {
                        tvResult.text = JSONTemplate.parseJson(result)
                    } else {
                        tvResult.text = "没有返回数据"
                    }
                }
            }
            else -> {
                // 网页展示
                cvResult.loadUrl(url)
                tvResult.visibility = View.GONE
                cvResult.visibility = View.VISIBLE
            }
        }

    }

}