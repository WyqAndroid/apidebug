package com.android.mainproject

import java.io.UnsupportedEncodingException
import javax.crypto.Cipher
import javax.crypto.spec.IvParameterSpec
import javax.crypto.spec.SecretKeySpec

/**
 * 创建人： 汪勇奇
 * 创建时间：2018/7/6.
 * 文件说明：AES加密/解密工具类
 */
object AesEncryptUtil {

    /**
     * 创建AES秘钥
     */
    private fun createAesKey(password: String?): SecretKeySpec {
        var password = password
        var data: ByteArray? = null
        if (password == null) {
            password = ""
        }
        val sb = StringBuilder(32)
        sb.append(password)
        while (sb.length < 32) {
            sb.append("0")
        }
        if (sb.length > 32) {
            sb.setLength(32)
        }
        try {
            data = sb.toString().toByteArray(charset("UTF-8"))
        } catch (e: UnsupportedEncodingException) {
        }
        return SecretKeySpec(data, "AES")
    }

    /**
     * 返回加密或解密的字节
     * mode:加密or解密模式
     * AES文本加密/解密 秘钥为32位字符串
     */
    private fun getAesEncryptByte(b: ByteArray, key: String, mode: Int): ByteArray? {
        try {
            val k = createAesKey(key)
            val cipher = Cipher.getInstance("AES/CFB/NoPadding")
            // 使用CFB加密，需要设置IV
            cipher.init(mode, k, IvParameterSpec(
                    ByteArray(cipher.blockSize)))
            return cipher.doFinal(b)
        } catch (e: Exception) {
        }
        return null
    }

    /**
     * 字节转16进制
     */
    private fun byteToHex(b: ByteArray): String {
        val sb = StringBuilder(b.size * 2)
        try {
            for (aB in b) {
                // 整数转成十六进制
                var tmp = Integer.toHexString(aB.toInt() and 0XFF)
                if (tmp.length == 1) {
                    sb.append("0")
                }
                sb.append(tmp)
            }
        } catch (ex: Exception) {
        }
        // 转成大写
        return sb.toString().toUpperCase()
    }

    /**
     * AES加密字符串
     */
    fun aesEncryptString(text: String, key: String): String {
        var b = text.toByteArray(charset("UTF-8"))
        var data = getAesEncryptByte(b, key, Cipher.ENCRYPT_MODE)
        return if (data == null) {
            ""
        } else {
            byteToHex(data)
        }
    }

}