package com.android.mainproject

import android.Manifest
import android.content.pm.PackageManager
import android.os.Build
import android.os.Environment
import android.support.v4.content.ContextCompat
import android.text.TextUtils
import java.io.BufferedWriter
import java.io.File
import java.io.FileOutputStream
import java.io.OutputStreamWriter

/**
 * 创建人： 汪勇奇
 * 创建时间：2018/7/30.
 * 文件说明：
 */

object LogUtil {

    /**
     * 保存日志
     *
     * @param url
     */
    fun saveLog(url: String) {
        if (!TextUtils.isEmpty(url)) {
            val text = "type=Get;url=" + url + ";time=" + System.currentTimeMillis()
            save(text)
        }
    }

    /**
     * 保存日志
     *
     * @param url
     * @param parameter
     */
    fun saveLog(url: String, parameter: String) {
        if (!TextUtils.isEmpty(url)) {
            val text = "type=Post;url=" + url + ";parameter=" + parameter + ";time=" + System.currentTimeMillis()
            save(text)
        }
    }

    /**
     * 保存文本
     *
     * @param text
     */
    @Synchronized
    private fun save(text: String) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && ContextCompat.checkSelfPermission(App.context,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            // 没有权限
            return
        }
        try {
            if (!TextUtils.isEmpty(text)) {
                val aesEncryptString = AesEncryptUtil.aesEncryptString(text, "ZheLiXieNiDeMiYao")
                if (android.os.Environment.getExternalStorageState() == android.os.Environment.MEDIA_MOUNTED) {
                    val fileDir = File(Environment.getExternalStorageDirectory().canonicalPath + "/Log/encrypt/")
                    if (!fileDir.exists()) {
                        fileDir.mkdirs()
                    }
                    val file = File(fileDir, System.currentTimeMillis().toString() + ".txt")
                    if (file.exists()) {
                        file.delete()
                    }
                    val out = BufferedWriter(OutputStreamWriter(FileOutputStream(file)))
                    out.write(aesEncryptString)
                    out.flush()
                    out.close()
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

}